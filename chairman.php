<!DOCTYPE html>
<html>
<head>
    <title>Chairman's page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>   
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Bootstrap CDNs -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <style type="text/css">
    .topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }

  .card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.price {
  color: grey;
  font-size: 22px;
}

.card button {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

.card button:hover {
  opacity: 0.7;
}
  </style>
</head>
<body>

<div class="jumbotron" style="padding: 50px;">
    
    <table >
        <tr>
  <td><img src="tuklogo.png" height="100px" width="340px"></td>
  <td><blockquote style="margin-left:250px;">
        <img src="scit.jpg" style=" height: 140px; width: 190px;" >
    </blockquote></td>
    </tr><br>

    
  <tr>
  <td><a href="logout.php" class="btn btn-danger" style="font-size: 15px; margin-left: 55px; ">LOG OUT</a></td>
  </tr> 
  </table>
<!-- Search -->
  <span><input type="text"  name="" placeholder="    Search...." style="box-shadow: 0 15px 20px rgba(0, 0, 0, 0.3); border-color: #4d4dff; border-radius: 5px;margin-left: 600px; width: 200px; height: 30px; font-size: 15px;"></span><button type="submit" style="border-color: #4d4dff; border-radius: 5px;"><i class="fa fa-search" style="padding-top: 10px; padding-bottom: 5px;"></i></button>
<!-- end of search -->
</div>
<div class="jumbotron" style="font-size: 14px; width: 60%; margin-left: 20%; margin-right: 20%">
      
    <!-- container -->
    <div class="container">
     
        <!-- PHP code to read records will be here where role is director -->

        <?php
            // include database connection
            include 'database.php';
             
            // delete message prompt will be here

            $action = isset($_GET['action']) ? $_GET['action'] : "";
 
            // if it was redirected from delete.php
            if($action=='deleted'){
                echo "<div class='alert alert-success'>Record was deleted.</div>";
            }

            // end of delete message prompt
            
            //fname=:fname, mname=:mname, lname=:lname, nid=:nid, email=:email, pno=:pno, dept=:dept, role=:role, uname=:uname, psw=:psw
            // select all data
            $query = "SELECT id, fname, mname, lname, dept, role FROM users where role='lecturer' OR role='student' ORDER BY id DESC";
            $stmt = $con->prepare($query);
            $stmt->execute();
             
            // this is how to get number of rows returned
            $num = $stmt->rowCount();
             
            // link to create record form
            echo "<td>
            <a href='add_lec.php' class='btn btn-primary m-b-1em' style='font-size:14px; margin-bottom:20px;'>
            Add lecturer/student</a>";
            
            //check if more than 0 record found
            if($num>0){
             
                // data from database will be here

                echo "<table class='table table-hover table-responsive table-bordered' style='width:95%; margin-left:2.5%; margin-right:2.5%; font-size:14px;'>";//start table
                
                //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                //creating our table heading
                echo "<tr>";
                    echo "<th>ID</th>";
                    echo "<th>First name</th>";
                    echo "<th>Middle name</th>";
                    echo "<th>Last name</th>";
                    echo "<th>Department</th>";
                    echo "<th>Role</th>";
                echo "</tr>";
     
                // table body will be here

                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['firstname'] to
                    // just $firstname only
                    extract($row);
                    
                    //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                    // creating new table row per record
                    echo "<tr>";
                        echo "<td>{$id}</td>";
                        echo "<td>{$fname}</td>";
                        echo "<td>{$mname}</td>";
                        echo "<td>{$lname}</td>";
                        echo "<td>{$dept}</td>";
                        echo "<td>{$role}</td>";
                        echo "<td>";
                            // read one record 
                            echo "<a href='more_chair.php?id={$id}' class='btn btn-info m-r-1em'
                            style='font-size:14px; margin-left:10px; margin-right:10px; padding:6px;'>More</a>";
                             
                            // we will use this links on next part of this post
                            echo "<a href='edit_chair.php?id={$id}' class='btn btn-primary m-r-1em'
                            style='font-size:14px; margin-left:10px; margin-right:10px; padding:6px;'>Edit</a>";
                 
                            // we will use this links on next part of this post
                            echo "<a href='#' onclick='delete_user({$id});'  class='btn btn-danger'
                            style='font-size:14px; margin-left:10px; margin-right:10px; padding:6px;'>Delete</a>";
                        echo "</td>";
                    echo "</tr>";
                }

                // end of table body
 
                // end table
                echo "</table>";

                // end of data from database
                 
            }
             
            // if no records found
            else{
                echo "<div class='alert alert-danger'>No records found.</div>";
            }
        ?>

        <!-- end of read code -->
         
    </div> <!-- end .container -->

    <!-- PHP code to read records will be here where role is other -->

        <?php
            // include database connection
            include 'database.php';

            // end of delete message prompt
            
            //fname=:fname, mname=:mname, lname=:lname, nid=:nid, email=:email, pno=:pno, dept=:dept, role=:role, uname=:uname, psw=:psw
            // select all data
            $query = "SELECT id, fname, mname, lname, dept, role FROM users WHERE role='director' OR role='chairman' ORDER BY id DESC";
            $stmt = $con->prepare($query);
            $stmt->execute();
             
            // this is how to get number of rows returned
            $num = $stmt->rowCount();
            
            //check if more than 0 record found
            if($num>0){
             
                // data from database will be here

                echo "<table class='table table-hover table-responsive table-bordered' style='width:91%; margin-left:4.5%; margin-right:4.5%; font-size:14px;'>";//start table
                
                //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                //creating our table heading
                echo "<tr>";
                    echo "<th>ID</th>";
                    echo "<th>First name</th>";
                    echo "<th>Middle name</th>";
                    echo "<th>Last name</th>";
                    echo "<th>Department</th>";
                    echo "<th>Role</th>";
                echo "</tr>";
     
                // table body will be here

                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['firstname'] to
                    // just $firstname only
                    extract($row);
                    
                    //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                    // creating new table row per record
                    echo "<tr>";
                        echo "<td>{$id}</td>";
                        echo "<td>{$fname}</td>";
                        echo "<td>{$mname}</td>";
                        echo "<td>{$lname}</td>";
                        echo "<td>{$dept}</td>";
                        echo "<td>{$role}</td>";
                        echo "<td>";
                            // read one record 
                            // echo "<a href='more_dir.php?id={$id}' class='btn btn-info m-r-1em'
                            // style='font-size:14px; margin-left:10px; margin-right:10px; padding:6px;'>More</a>";
                             
                            // we will use this links on next part of this post
                            // echo "<a href='edit_dir.php?id={$id}' class='btn btn-primary m-r-1em'
                            // style='font-size:14px; margin-left:10px; margin-right:10px; padding:6px;'>Edit</a>";
                 
                            // we will use this links on next part of this post
                            // echo "<a href='#' onclick='delete_user({$id});'  class='btn btn-danger'
                            // style='font-size:14px; margin-left:10px; margin-right:10px;  padding:6px;'>Delete</a>";
                            // echo "</td>";
                    echo "</tr>";
                }

                // end of table body
 
                // end table
                echo "</table>";

                // end of data from database
                 
            }
             
            // if no records found
            else{
                echo "<div class='alert alert-danger'>No records found.</div>";
            }
        ?>

        <!-- end of read code -->
         
    </div> <!-- end .container -->
     
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
<!-- confirm delete record will be here -->

<script type='text/javascript'>
// confirm record deletion
function delete_user( id ){
     
    var answer = confirm('Are you sure?');
    if (answer){
        // if user clicked ok, 
        // pass the id to delete.php and execute the delete query
        window.location = 'del_dir.php?id=' + id;
    } 
}
</script>

<!-- end of confirm delete record code -->
 
</body>
</html>