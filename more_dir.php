<!DOCTYPE HTML>
<html>
<head>
    <title>More</title>
 
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
 
</head>
<body>
 
 
    <!-- container -->
    <div class="container">
  
        <div class="page-header">
            <h1>More on director</h1>
        </div>
         
        <!-- PHP read one record will be here -->

        <?php
            // get passed parameter value, in this case, the record ID
            // isset() is a PHP function used to verify if a value is there or not
            $id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
             
            //include database connection
            include 'database.php';
             
            // read current record's data
            try {
                // prepare select query
                $query = "SELECT id, fname, mname, lname, nid, email, pno, dept, role, uname,psw FROM users WHERE id = ? LIMIT 0,1";
                $stmt = $con->prepare( $query );
             
                // this is the first question mark
                $stmt->bindParam(1, $id);
             
                // execute our query
                $stmt->execute();
             
                // store retrieved row to a variable
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
             
                // values to fill up our form
                $fname = $row['fname'];
                $mname = $row['mname'];
                $lname = $row['lname'];
                $nid = $row['nid'];
                $email = $row['email'];
                $pno = $row['pno'];
                $dept = $row['dept'];
                $role = $row['role'];
                $uname = $row['uname'];
                $psw = $row['psw'];

            }
             
            // show error
            catch(PDOException $exception){
                die('ERROR: ' . $exception->getMessage());
            }
        ?>

        <!-- end of php read one record code -->
 
        <!-- HTML read one record table will be here -->

        <!--we have our html table here where the record will be displayed-->
            <table class='table table-hover table-responsive table-bordered'>
                <tr>
                    <td>First name</td>
                    <td><?php echo htmlspecialchars($fname, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Middle name</td>
                    <td><?php echo htmlspecialchars($mname, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Last name</td>
                    <td><?php echo htmlspecialchars($lname, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>National ID</td>
                    <td><?php echo htmlspecialchars($nid, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo htmlspecialchars($email, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Phone number</td>
                    <td><?php echo htmlspecialchars($pno, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Department</td>
                    <td><?php echo htmlspecialchars($dept, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td><?php echo htmlspecialchars($role, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?php echo htmlspecialchars($uname, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><?php echo htmlspecialchars($psw, ENT_QUOTES);  ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href='admin.php' class='btn btn-danger'>Back to home page</a>
                    </td>
                </tr>
            </table>

        <!-- end of HTML record table code -->
 
    </div> <!-- end .container -->
     
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</body>
</html>