<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<form method="post">
  <table class="loginTable">
     <tr>
      <th>ADMIN PANEL LOGIN</th>
     </tr>
     <tr>
      <td>
        <label class="firstLabel">Username:</label>
        <input type="text" name="uname" id="uname" value="" autocomplete="off" />
      </td>
     </tr>
     <tr>
      <td><label>Password:</label>
        <input type="psw" name="psw" id="psw" value="" autocomplete="off" /></td>
     </tr>
     <tr>
      <td>
         <input type="submit" name="submitBtnLogin" id="submitBtnLogin" value="Login" />
         <span class="loginMsg"><?php echo @$msg;?></span>
      </td>
     </tr>
  </table>
</form>

<?php 
session_start();
include("db.php");
?>
<?php
$msg = ""; 
if(isset($_POST['submitBtnLogin'])) {
  $uname = trim($_POST['uname']);
  $psw = trim($_POST['psw']);
  if($uname != "" && $psw != "") {
    try {
      $query = "SELECT * from `users` where `uname`=:uname and `psw`=:psw";
      $stmt = $db->prepare($query);
      $stmt->bindParam('uname', $uname, PDO::PARAM_STR);
      $stmt->bindValue('psw', $psw, PDO::PARAM_STR);
      $stmt->execute();
      $count = $stmt->rowCount();
      $row   = $stmt->fetch(PDO::FETCH_ASSOC);
      if($count == 1 && !empty($row)) {
        /******************** Your code ***********************/
        $_SESSION['sess_user_id']   = $row['id'];
        $_SESSION['sess_user_name'] = $row['uname'];
        $_SESSION['sess_name'] = $row['psw'];
       
      } else {
        $msg = "Invalid username and password!";
      }
    } catch (PDOException $e) {
      echo "Error : ".$e->getMessage();
    }
  } else {
    $msg = "Both fields are required!";
  }
}
?>

</body>
</html>



