<!DOCTYPE HTML>
<html>
<head>
    <title>Edit director</title>
     
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
         
</head>
<body>
    

    <!-- container -->
    <div class="container">
   
        <div class="page-header">
            <h1>Edit director</h1>
        </div>
      
    <!-- html form to create product will be here -->
          
    </div> <!-- end .container -->


    <!-- container -->
    <div class="container" style="background-color: #cccccc;">
  
        
     
        <!-- PHP read record by ID will be here -->

       <?php
            // get passed parameter value, in this case, the record ID
            // isset() is a PHP function used to verify if a value is there or not
            $id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
             
            //include database connection
            include 'database.php';
             
            // read current record's data
            try {
                // prepare select query
                $query = "SELECT id, fname, mname, lname, nid, email, pno, dept, role, uname, psw FROM users WHERE id = ? LIMIT 0,1";
                $stmt = $con->prepare( $query );
                 
                // this is the first question mark
                $stmt->bindParam(1, $id);
                 
                // execute our query
                $stmt->execute();
                 
                // store retrieved row to a variable
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                 
                // values to fill up our form
                $fname = $row['fname'];
                $mname = $row['mname'];
                $lname = $row['lname'];
                $nid = $row['nid'];
                $email = $row['email'];
                $pno = $row['pno'];
                $dept = $row['dept'];
                $role = $row['role'];
                $uname = $row['uname'];
                $psw = $row['psw'];
            }
             
            // show error
            catch(PDOException $exception){
                die('ERROR: ' . $exception->getMessage());
            }
        ?>

        <!-- end of PHP read record code -->
 
        <!-- HTML form to update record will be here -->

        <!-- PHP post to update record will be here -->

        <?php
 
            // check if form was submitted
            if($_POST){
                 
                try{
                 
                    // write update query
                    // in this case, it seemed like we have so many fields to pass and 
                    // it is better to label them and not use question marks
                    $query = "UPDATE users 
                                SET fname=:fname, mname=:mname, lname=:lname, nid=:nid, email=:email, pno=:pno, dept=:dept, role=:role, uname=:uname, psw=:psw
                                WHERE id = :id";
             
                    // prepare query for excecution
                    $stmt = $con->prepare($query);
             
                    // posted values
                    $fname=htmlspecialchars(strip_tags($_POST['fname']));
                    $mname=htmlspecialchars(strip_tags($_POST['mname']));
                    $lname=htmlspecialchars(strip_tags($_POST['lname']));
                    $nid=htmlspecialchars(strip_tags($_POST['nid']));
                    $email=htmlspecialchars(strip_tags($_POST['email']));
                    $pno=htmlspecialchars(strip_tags($_POST['pno']));
                    $dept=htmlspecialchars(strip_tags($_POST['dept']));
                    $role=htmlspecialchars(strip_tags($_POST['role']));
                    $uname=htmlspecialchars(strip_tags($_POST['uname']));
                    $psw=htmlspecialchars(strip_tags($_POST['psw']));
             
                    // bind the parameters
                    $stmt->bindParam(':fname', $fname);
                    $stmt->bindParam(':mname', $mname);
                    $stmt->bindParam(':lname', $lname);
                    $stmt->bindParam(':nid', $nid);
                    $stmt->bindParam(':email', $email);
                    $stmt->bindParam(':pno', $pno);
                    $stmt->bindParam(':dept', $dept);
                    $stmt->bindParam(':role', $role);
                    $stmt->bindParam(':uname', $uname);
                    $stmt->bindParam(':psw', $psw);
                    $stmt->bindParam(':id', $id);
                     
                    // Execute the query
                    if($stmt->execute()){
                        echo "<div class='alert alert-success'>Record was updated.</div>";
                    }else{
                        echo "<div class='alert alert-danger'>Unable to update record. Please try again.</div>";
                    }
                     
                }
                 
                // show errors
                catch(PDOException $exception){
                    die('ERROR: ' . $exception->getMessage());
                }
            }
        ?>
        <!-- end of php post to update records -->
 
<!--we have our html form here where new record information can be updated-->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}");?>" method="post">
    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <td>First name</td>
            <td><input type='text' name='fname' value="<?php echo htmlspecialchars($fname, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Middle name</td>
            <td><input type='text' name='mname' value="<?php echo htmlspecialchars($mname, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Last name</td>
            <td><input type='text' name='lname' value="<?php echo htmlspecialchars($lname, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>National ID</td>
            <td><input type='text' name='nid' value="<?php echo htmlspecialchars($nid, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type='text' name='email' value="<?php echo htmlspecialchars($email, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Phone number</td>
            <td><input type='text' name='pno' value="<?php echo htmlspecialchars($pno, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Department</td>
            <td><input type='text' name='dept' value="<?php echo htmlspecialchars($dept, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Role</td>
            <td><input type='text' name='role' value="<?php echo htmlspecialchars($role, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Username</td>
            <td><input type='text' name='uname' value="<?php echo htmlspecialchars($uname, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type='text' name='psw' value="<?php echo htmlspecialchars($psw, ENT_QUOTES);  ?>" class='form-control' /></td>
        </tr>                  
        
    </table>
</div> <!-- end .container -->
    <br>
<table>
    <tr>
        <td>
            <div style="margin-left: 975px; margin-bottom: 20px;">
            <input type='submit' value='Save Changes' class='btn btn-primary'/>
            <a href='admin.php' class='btn btn-danger'>Back to home page</a>
            </div>
        </td>
    </tr>
</table>
</form>
        <!-- end of HTML form -->     
    

    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</body>
</html>