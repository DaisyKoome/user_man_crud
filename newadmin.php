<?php
require('php/db.php');
?>

<!DOCTYPE html>
<html>
<head>
  <title>Salvation</title>
  <!-- Metas -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CDNs -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<style type="text/css">
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 50px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav li:hover {
  color: #f1f1f1;
}

.sidenav body{
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


.flip-card {
  background-color: transparent;
  width: 320px;
  height: 320px;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 320px;
  height: 320px;
  text-align: center;
  transition: transform 2s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
  transition: 3s;
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 320px;
  height: 320px;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: #bbb;
  color: black;
}

.flip-card-back {
  background-color: #2980b9;
  color: white;
  transform: rotateY(180deg);
}

.container1 {
  position: relative;
  width: 320px;
  margin: 0 auto;
}

.container1 img {vertical-align: middle;}

.container1 .content {
  position: absolute;
  bottom: 0;
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1;
  width: 304px;
  margin-left: 8px;
  padding: 3px;
}


:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}


* {
  box-sizing: border-box;
}

img {
  vertical-align: middle;
}

/* Position the image container (needed to position the left and right arrows) */
.container {
  position: relative;
}

/* Hide the images by default */
.mySlides {
  display: none;
}

/* Add a pointer when hovering over the thumbnail images */
.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 40%;
  width: auto;
  padding: 10px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Container for image text */
.caption-container {
  text-align: center;
  background-color: #222;
  padding: 2px 16px;
  color: white;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Six columns side by side */
.column {
  float: left;
  width: 16.66%;
}

/* Add a transparency effect for thumnbail images */
.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}
/*body {
  background: #007bff;
  background: linear-gradient(to right, #0062E6, #33AEFF);
}*/


.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: #777;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
}

.panel {
  padding: 0 13px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}



</style>


</head>
<body>

<div class="jumbotron" style="padding: 50px;">

<table>
    <tr>
  <table >
    <tr>
  
  <!-- Collapsible side nav -->
  <td>
    <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <ul>
  <li><a href="landing.html">Home</a></li>
  <li><a href="who_is_God2.html">Who is God?</a></li>
  <li><a href="who_is_jesus.html">Who is Jesus?</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/faithreg.php"
  onclick="closeNav()">Faith</a></li>
  <li><a href="http://localhost:8080/the-believer/Believer_web_app2/php/db_registration.php" 
  onclick="closeNav()">Who am I?</a></li>
  </ul>
</div>

<div id="main">
  
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
</div>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.body.style.backgroundColor = "white";
}
</script>
   
  </td>
<!-- end of nav -->

<!-- Log in modal -->

<body>





  <!-- The Modal -->
  

  
<!-- end of log in modal -->

  <td><blockquote style="text-align: center;margin-left: 450px;"><h1>#Tujue_Word</h1> </blockquote></td>
  </tr>
  </table>
</div>
</tr>


<h2 style="text-align:center">Salvation</h2>

<div class="container" style="margin-left: 7%;">
  <div class="mySlides">
  
    <!-- <img src="wp12.jpg" style="width:1110px;"> -->
     <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">What does it really mean to be saved?</h1>
    <p class="lead">10 For it is with your heart that you believe and are justified, <br> and it is with your mouth that you profess your faith and are saved.<br>
    <?php  
        $query="SELECT * FROM links where id=1";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
  </div>
  </div>


  <div class="mySlides">
    <div class="numbertext">2 / 6</div>
    <!-- <img src="wp13.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">Does salvation have a price tag on it?</h1>
    <p class="lead">8 For it is by grace you have been saved, through faith— <br> and this is not from yourselves, it is the gift of God<br>
    <?php  
        $query="SELECT * FROM links where id=2";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">3 / 6</div>
    <!-- <img src="wp14.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">What benefit is there in salvation?</h1>
    <p class="lead"> 28 “Come to me, all you who are weary and burdened, <br>
      and I will give you rest. 29 Take my yoke upon you and learn from me, <br>
      for I am gentle and humble in heart, and you will find rest for your souls. <br>
      30 For my yoke is easy and my burden is light.” <br> 
      <?php  
        $query="SELECT * FROM links where id=3";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <div class="mySlides">
    <div class="numbertext">4 / 6</div>
    <!-- <img src="wp15.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">God's will -> that all men be saved</h1>
    <p class="lead">3This is good and pleasing in the sight of God our Savior, <br>
    4 who desires all men to be saved and to come to the knowledge of the truth. <br> 
    <?php  
        $query="SELECT * FROM links where id=4";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?> 
    </p>
  </div>
</div>
  </div>

  <div class="mySlides">
    <div class="numbertext">5 / 6</div>
    <!-- <img src="wp16.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
    <h1 class="display-4">Jesus! The only name by which we are saved</h1>
    <p class="lead">11 Jesus is “‘the stone you builders rejected, <br>
    which has become the cornerstone.’[a] <br>
    12 Salvation is found in no one else, <br> 
    for there is no other name under heaven <br>
    given to mankind by which we must be saved.” <br>
    <?php  
        $query="SELECT * FROM links where id=5";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <div class="mySlides">
    <div class="numbertext">6 / 6</div>
    <!-- <img src="wp17.jpg" style="width:1110px;"> -->
    <div class="jumbotron jumbotron-fluid">
  <div class="container" style="margin-left: 30px;">
     <h1 class="display-4">Work out your salvation</h1>
    <p class="lead"> Dear friends, you always followed my instructions when I was with you. <br> 
      And now that I am away, it is even more important. <br> 
      Work hard to show the results of your salvation, <br> 
      obeying God with deep reverence and fear. <br> 
      <?php  
        $query="SELECT * FROM links where id=6";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $verse=$row['salvation'];
        }
        echo $verse;
        //echo "<img src='".$image. "' alt='' height='200' width='250' />";
    ?>
    </p>
  </div>
</div>
  </div>
    
  <a class="prev" onclick="plusSlides(-1)">❮</a>
  <a class="next" onclick="plusSlides(1)" style="margin-right: 18px;">❯</a>


 <div class="caption-container" style="padding-top: 0px; margin-top: 0px;">
    <p id="caption"></p>
  </div>

  <div class="row" style="width: 100%; margin-left: 2px;">
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=7";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='Salvation -> believing in Christ and confessing it' height='150' width='180' onclick='currentSlide(1)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=8";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='Priceless but given so freely' height='150' width='180' onclick='currentSlide(2)' class='demo cursor' />";
      ?> 
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=9";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='Come to Jesus for rest' height='150' width='180' onclick='currentSlide(3)' class='demo cursor' />";
      ?>  
      
    </div>
    <div class="column">
       
      <?php  
        $query="SELECT * FROM links where id=10";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='God wants ALL men to be saved, no exceptions' height='150' width='180' onclick='currentSlide(4)' class='demo cursor' />";
      ?>  
      
    </div>
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=11";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='Jesus is the way, the truth and the life' height='150' width='180' onclick='currentSlide(5)' class='demo cursor' />";
      ?>  
      
    </div>    
    <div class="column">
      
      <?php  
        $query="SELECT * FROM links where id=12";
        $result=mysqli_query($con,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['salvation'];
        }
        echo "<img src='".$image. "' alt='Toil to keep your salvation thriving' height='150' width='180' onclick='currentSlide(6)' class='demo cursor' />";
      ?>  
      
    </div>
  </div>
</div>
<br><br>
<!--  Sermons -->
<div class="container" style="margin-left: 7%;">
<blockquote><h3>#Sermons to top it all off!!</h3></blockquote>
<div class="jumbotron" style="padding: 50px;">
  <table>
    <tr>
      <td>
        <blockquote> The late Dr Myles Munroe <br> Principles of Salvation and the Kingdom </blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/fsxGnivPMcg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Joyce Meyer <br> What Is Salvation?</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/0200iPCZ7jw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>We are free from the slavery of sin and Satan <br> The late Dr. Myles Munroe</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/vXAswNaAM1k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
    <tr>
      <td>
        <blockquote class="accordion">Forgive Me God, Change Me <br> Joyce Meyer</blockquote>
        <div class="panel">
       <iframe width="320" height="240" src="https://www.youtube.com/embed/Yv47bsO9b5U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </td>
      <td>
         <blockquote class="accordion">Understanding Jesus as Lord <br> by the late Dr Myles Munroe</blockquote>
         <div class="panel">
        <iframe width="320" height="240" src="https://www.youtube.com/embed/FS0OQaSWOL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
         </div>
      </td>
      <td style="margin-left: 20px;">
        <blockquote class="accordion">Receive Christ <br> by Joyce Meyer</blockquote>
        <div class="panel">
        <iframe width="320" height="240" src="https://www.youtube.com/embed/gnJhIqHp3vw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </td>
    </tr>
  </table>
</div>
</div>


<!--  Music videos
<div class="container" style="margin-left: 7%;">
<blockquote><h3>#Godly musicals</h3></blockquote>
<div class="jumbotron" style="padding: 50px;">
  <table>
    <tr>
      <td>
        <blockquote>Salvation Belongs To Our God (lyrics) <br> By Adrian Howard and Pat Turner</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/RihfmBH8JbQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Wonderful Merciful Savior (Official Video) <br> By Selah</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/fK6sYVQCqhs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Mighty to Save <br> By Hillsong Worship</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/GEAcs2B-kNc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
    <tr>
      <td>
        <blockquote>Lauren Daigle <br> Rescue (Audio)</blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/3U4Q2R7ZZAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td>
        <blockquote>Lauren Daigle <br> Rebel Heart (Audio)</blockquote>
       <iframe width="320" height="240" src="https://www.youtube.com/embed/sufQX7NSX2k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
      <td style="margin-left: 20px;">
        <blockquote>The Walls Group <br> Perfect People </blockquote>
        <iframe width="320" height="240" src="https://www.youtube.com/embed/_pp37j1QURI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </td>
    </tr>
  </table>
</div>
</div> -->

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
    

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

</body>
</html>