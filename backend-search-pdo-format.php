<!DOCTYPE html>
<html>
<head>
    <title></title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>   
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Bootstrap CDNs -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
try{
    $pdo = new PDO("mysql:host=localhost;dbname=crud", "root", "");
    // Set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
    die("ERROR: Could not connect. " . $e->getMessage());
}
 
// Attempt search query execution
try{
    if(isset($_REQUEST["term"])){
        // create prepared statement
        $sql = "SELECT * FROM users WHERE nid LIKE :term";
        $stmt = $pdo->prepare($sql);
        $term = $_REQUEST["term"] . '%';
        // bind parameters to statement
        $stmt->bindParam(":term", $term);
        // execute the prepared statement
        $stmt->execute();
        if($stmt->rowCount() > 0){
            while($row = $stmt->fetch()){
                echo "<br>";
                 echo "<table class='table table-hover table-responsive table-bordered' style='font-family:century gothic; font-size:16px;'>";//start table
                echo "<tr>"."<th>".
                 "National ID"."</th>";
                //echo "<td>"."<p style='margin-left:50px;'>" . $row["nid"] . "</p>"."</td>";

                 echo "<th>"."First name"."</th>";
                //echo "<td>"."<p style='margin-left:50px;'>" . $row["fname"] . "</p>"."</td>";
                 //id, fname, mname, lname, nid, email, pno, dept, role, uname, psw
                echo "<th>"."Middle name"."</th>";
                echo "<th>"."Last name"."</th>";
                echo "<th>"."Email"."</th>";
                echo "<th>"."Phone number"."</th>";
                echo "<th>"."Department"."</th>";
                echo "<th>"."Role"."</th>";
                echo "<th>"."Username"."</th>";
                echo "<th>"."password"."</th>";
                echo "</tr>";

                echo "<tr>";
                        echo "<td>".$row["nid"]."</td>";
                        echo "<td>".$row["fname"]."</td>";
                        echo "<td>".$row["mname"]."</td>";
                        echo "<td>".$row["lname"]."</td>";
                        echo "<td>".$row["email"]."</td>";
                        echo "<td>".$row["pno"]."</td>";
                        echo "<td>".$row["dept"]."</td>";
                        echo "<td>".$row["role"]."</td>";
                        echo "<td>".$row["uname"]."</td>";
                        echo "<td>".$row["psw"]."</td>";
                echo "</tr>";
                echo "</table>";
            }
        } else{
            echo "<p>No matches found</p>";
        }
    }  
} catch(PDOException $e){
    die("ERROR: Could not able to execute $sql. " . $e->getMessage());
}
 
// Close statement
unset($stmt);
 
// Close connection
unset($pdo);
?>

</body>
</html>