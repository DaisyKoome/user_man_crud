<?php 
session_start(); 
include("db2.php");
?>
<?php
$msg = ""; 
if(isset($_POST['submitBtnLogin'])) {
	$uname = trim($_POST['uname']);
	$password = ($_POST['psw']);
	$psw = md5($password);
	if($uname != "" && md5($psw) != "") 
		{
		try {
		//for admin
			$query = "SELECT * from `users` where `uname`=:uname and `psw`=:psw and `role`='admin'";
			$stmt = $db->prepare($query);
			$stmt->bindParam('uname', $uname, PDO::PARAM_STR);
			$stmt->bindValue('psw', $psw, PDO::PARAM_STR);
			$stmt->execute();
			$count = $stmt->rowCount();
			$row   = $stmt->fetch(PDO::FETCH_ASSOC);
			if($count == 1 && !empty($row)) {
				/******************** Your code ***********************/
				$_SESSION['sess_user_id']   = $row['id'];
				$_SESSION['sess_username'] = $row['uname'];
				$_SESSION['sess_name'] = $row['fname'];
				header('location:admin.php');
			} else {
				$msg = "Invalid username and password!";
			}
		//end of admin

		//for director
			$query = "SELECT * from `users` where `uname`=:uname and `psw`=:psw and `role`='director'";
			$stmt = $db->prepare($query);
			$stmt->bindParam('uname', $uname, PDO::PARAM_STR);
			$stmt->bindValue('psw', $psw, PDO::PARAM_STR);
			$stmt->execute();
			$count = $stmt->rowCount();
			$row   = $stmt->fetch(PDO::FETCH_ASSOC);
			if($count == 1 && !empty($row)) {
				/******************** Your code ***********************/
				$_SESSION['sess_user_id']   = $row['id'];
				$_SESSION['sess_username'] = $row['uname'];
				$_SESSION['sess_name'] = $row['fname'];
				header('location:dir.php');
			} else {
				$msg = "Invalid username and password!";
			}
		//end of director

		//for director
			$query = "SELECT * from `users` where `uname`=:uname and `psw`=:psw and `role`='chairman'";
			$stmt = $db->prepare($query);
			$stmt->bindParam('uname', $uname, PDO::PARAM_STR);
			$stmt->bindValue('psw', $psw, PDO::PARAM_STR);
			$stmt->execute();
			$count = $stmt->rowCount();
			$row   = $stmt->fetch(PDO::FETCH_ASSOC);
			if($count == 1 && !empty($row)) {
				/******************** Your code ***********************/
				$_SESSION['sess_user_id']   = $row['id'];
				$_SESSION['sess_username'] = $row['uname'];
				$_SESSION['sess_name'] = $row['fname'];
				header('location:chairman.php');
			} else {
				$msg = "Invalid username and password!";
			}
		//end of director

		} catch (PDOException $e) {
			echo "Error : ".$e->getMessage();
		}
	} 
	else {
		$msg = "Both fields are required!";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Login</title>
<style>
.clearfix { clear:both; }
span { clear:both; display:block; margin-bottom:30px; }
span a { font-weight:bold; color:#0099FF; }
label { margin-top:20px; margin-bottom:3px; font-weight:bold;}
.loginTable {
 margin: auto;
 position:absolute;
 left:0;
 top:0;
 right:0;
 bottom:0;
 width:400px;
 height:200px;
 border:2px solid #0099FF;
 padding:10px;
}
.loginTable label {
	display:block;
	margin-bottom:3px;
	color:#0099FF;
	font-weight:bold;
}
.loginTable .firstLabel {
	margin-top:20px;
}

.loginTable th {
	border-bottom:2px solid #0099FF;
	margin-bottom:10px;
	color:#0099FF;
}
.loginTable #username, #password,#mname {
	width:365px;
	margin-bottom:7px;
	padding:3px 10px;
}
.loginTable #submitBtnLogin {
	padding:5px 20px;
}
.loginTable .loginMsg {
	color:#FF0000;
	text-align: center;
    padding-top: 5px;
	height:10px;
}
</style>

</head>

<body>
	
		<form method="post">
		<table class="loginTable">
		  <tr>
			<th>LOGIN</th>
		  </tr>
		  <div >
		  <tr>
			<td>
				<label class="firstLabel" style="margin-right: 40%; width: 20%; margin-left: 40%;">
				Username:</label>
		    	<input type="text" name="uname" id="uname" value="" autocomplete="off" 
		    	style="margin-right: 20%; width: 220px; margin-left: 20%;"/>
			</td>
		  </tr>
		  <tr>
			<td><label style="margin-right: 40%; width: 20%; margin-left: 40%;">
			Password:</label>
		    <input type="password" name="psw" id="psw" value="" autocomplete="off" 
		    style="margin-right: 20%; width: 220px; margin-left: 20%;"/></td>
		  </tr>
		  <tr>
			<td>
				<br>
				<input type="submit" name="submitBtnLogin" id="submitBtnLogin" value="Login" style="background-color:#99ebff; border-radius: 5px; border-color: #80e5ff;
					margin-right: 10%; width: 20%; margin-left: 70%;"/>
					<span class="loginMsg"><?php echo @$msg;?></span></td>
		  </tr>
		  </div>
		</table>
		</form>
	
</body>
</html>
