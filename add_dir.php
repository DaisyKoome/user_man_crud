<!DOCTYPE HTML>
<html>
<head>
    <title>Add director</title>
      
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>   
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Bootstrap CDNs -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<style type="text/css">
  
body {

      font-size: 16px;

     }

.container {

          font-size: 16px;

           }

.form-control {

            font-size: 14px;

              }
</style>

         
</head>
<body>
  
    <!-- container -->
    <div class="container">
   
        <div class="page-header">
            <h1>Add director</h1>
        </div>
      
    <!-- html form to create product will be here -->
          
    </div> <!-- end .container -->
      
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

<br>


<!-- PHP insert code will be here -->

<?php
if($_POST){
 
    // include database connection
    include 'database.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO users SET fname=:fname, mname=:mname, lname=:lname, nid=:nid, email=:email, pno=:pno, dept=:dept, role=:role, uname=:uname, psw=:psw, created=:created";
 
        // prepare query for execution
        $stmt = $con->prepare($query);
 
        // posted values
        $fname=htmlspecialchars(strip_tags($_POST['fname']));
        $mname=htmlspecialchars(strip_tags($_POST['mname']));
        $lname=htmlspecialchars(strip_tags($_POST['lname']));
        $nid=htmlspecialchars(strip_tags($_POST['nid']));
        $email=htmlspecialchars(strip_tags($_POST['email']));
        $pno=htmlspecialchars(strip_tags($_POST['pno']));
        $dept=htmlspecialchars(strip_tags($_POST['dept']));
        $role="director";
        $uname=htmlspecialchars(strip_tags($_POST['uname']));
        $psw=htmlspecialchars(strip_tags($_POST['psw']));
        $psw=md5($psw);
 

        // bind the parameters
        $stmt->bindParam(':fname', $fname);
        $stmt->bindParam(':mname', $mname);
        $stmt->bindParam(':lname', $lname);
        $stmt->bindParam(':nid', $nid);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':pno', $pno);
        $stmt->bindParam(':dept', $dept);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':uname', $uname);
        $stmt->bindParam(':psw', $psw);
         
        // specify when this record was inserted to the database
        $created=date('Y-m-d H:i:s');
        $stmt->bindParam(':created', $created);
         
        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}


?>

<!-- End of php insert code -->
 
<!-- html form here where the product information will be entered -->
<div class="container" style="background-color: #cccccc; width: 40%;">
<form style="width: 60%; margin-left: 20%; margin-right: 20%; "
action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>"  method="post">
  
   <div class="form-group">
    <i class="fa fa-user"></i>
    <label for="exampleInputPassword1">First name</label>
    <input type="text" class="form-control" id="exampleInputPassword1"
    name="fname" placeholder="Enter first name" required>
  </div>
   <div class="form-group">
    <i class="fa fa-user"></i>
    <label for="exampleInputPassword1">Middle name</label>
    <input type="text" class="form-control" id="exampleInputPassword1" 
    name="mname" placeholder="Enter middle name" required>
  </div>
   <div class="form-group">
    <i class="fa fa-user"></i>
    <label for="exampleInputPassword1">Last name</label>
    <input type="text" class="form-control" id="exampleInputPassword1" 
    name="lname" placeholder="Enter last name" required>
  </div>
   <div class="form-group">
    <label for="exampleInputPassword1">National ID</label>
    <input type="text" class="form-control" id="exampleInputPassword1" 
    name="nid" placeholder="Enter ID" required>
  </div>
  <div class="form-group">
    <i class="fa fa-at"></i>
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" 
    name="email" aria-describedby="emailHelp" placeholder="Enter email"
    required>  
  </div>
  <div class="form-group">
    <i class="fa fa-phone"></i>
    <label for="exampleInputPassword1">Phone number</label>
    <input type="text" class="form-control" id="exampleInputPassword1" 
    name="pno" placeholder="Enter phone number" required>
  </div>
  <div class="form-group">
    <i class="fas fa-school"></i>
    <label for="exampleInputPassword1">Department</label>
    <input type="text" class="form-control" id="exampleInputPassword1"
    name="dept" placeholder="Select department" required>
  </div>
  <!-- <div class="form-group">
    <i class="fa fa-tags"></i>
    <label for="exampleInputPassword1">Role</label>
    <input type="text" class="form-control" id="exampleInputPassword1"
    name="role" placeholder="Enter role">
  </div> -->
  
  <div class="form-group">
    <i class="fa fa-user"></i>
    <label for="exampleInputPassword1">Username</label>
    <input type="text" class="form-control" id="exampleInputPassword1"
    name="uname" placeholder="Enter username" required>
  </div>
  <div class="form-group">
    <i class="fa fa-key" aria-hidden="true"></i>
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1"
    name="psw" placeholder="Enter password" required>
  </div>
  <div class="form-group">
    <i class="fa fa-key" aria-hidden="true"></i>
    <label for="exampleInputPassword1">Password repeat</label>
    <input type="password" class="form-control" id="exampleInputPassword1" 
    placeholder="Enter password again" required>
  </div>
  <div class="form-check">
   <!--  <table><tr><td>
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    </td>
    <td style="margin-left: 50px;">
    <label class="form-check-label" for="exampleCheck1" style="margin-left: 20px; padding-top: 10px; font-size:12px; ">Remember me</label>
    </td></tr></table> -->
  </div><br>
<a href='admin.php?' class='btn btn-danger m-r-1em'
style='float: right; margin-bottom: 50px; width: 80px; height: 35px; margin-left: 5px; text-align: center;
 font-size: 14px;'>Back</a>
<button class="btn btn-success" style="float: right; margin-bottom: 50px; width: 80px; height: 35px; 
 font-size: 14px;">Save</button>
<br>
</form>
</div>

</body>
</html>